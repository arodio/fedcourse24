# Federated Learning & Data Privacy, 2024-2025
## General info
This is the lab course for the main course: 
[Federated Learning & Data Privacy, 2024-2025](http://www-sop.inria.fr/members/Giovanni.Neglia/federatedlearning/).  
Lab assistants: [Chuan Xu](https://sites.google.com/view/chuanxu), Francesco Diana.
During these practical sessions students will have the opportunity to train ML models 
in a distributed way on Inria scientific cluster.  
Participation to the labs will be graded.
## Prerequisites
Familiarity with PyTorch. 
## Class schedule
* TP1: 21/01/2025. Time: 09:00-12.00. Room: Lucioles Campus, 281.  
* TP2: TBD  
* TP3: TBD
## Course material
[GitLab page of the course](https://gitlab.inria.fr/arodio/FedCourse24).

## Setup
[How to create a conda environment](https://notes.inria.fr/s/33fQX7DNRx#)

