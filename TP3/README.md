# Federated Learning & Data Privacy, 2024-2025

## Third Lab - 11 February 2025

Welcome to the third lab session of the Federated Learning & Data Privacy course! Today we will see how to implement Federated Learning in real network systems.


### EXERCISE 5 - Federated Learning with Flower

**Objective**: Gain practical experience with the [Flower federated learning framework](https://flower.ai/docs/framework/index.html) by deploying a real, distributed federated learning experiment. Explore personalized federated learning algorithms and compare their performance against the standard FedAvg.

### EXERCISE 5.1 - Federated Learning on Real Networks

**Goal**: Deploy a federated learning system using PyTorch and Flower to understand the setup and execution of federated learning in a networked environment.

#### Setup

1. **Clone the Flower repository and set up the PyTorch quickstart example**:
    ```bash
    git clone --depth=1 https://github.com/adap/flower.git && mv flower/examples/quickstart-pytorch . && rm -rf flower && cd quickstart-pytorch
    ```

2. **Install the dependencies**:
    ```bash
    pip install -e .
    ```

#### Run Federated Learning

- **Launch the simulation**
    ```    
    flwr run .
    ```

- Observe the federated training process initiated by PyTorch through Flower.

You can find complete instructions [here](https://github.com/adap/flower/tree/main/examples/quickstart-pytorch).



### EXERCISE 5.2 - Tackling Data Heterogeneity with FedProx

**Objective**: Understand how the FedProx algorithm addresses the challenges posed by data heterogeneity in federated learning and compare its performance with the FedAvg algorithm.

- **FedProx Overview**: FedProx modifies the local training objective by introducing a proximal term, which aims to reduce local model drift by penalizing significant deviations from the global model. Review the FedProx algorithm [Federated Optimization in Heterogeneous Networks (Algorithm 2)](https://arxiv.org/abs/1812.06127).

- **Instructions**: Follow the tutorial on FedProx available at [Flower's documentation](https://flower.ai/docs/baselines/fedprox.html). Run experiments trying different number of rounds and different mu values.
- **Analysis**: Discuss the observed differences in performance between FedAvg and FedProx. Are there specific configurations (e.g., number of local epochs) where FedProx particularly outperforms FedAvg?


### BONUS EXERCISE - Personalized Federated Learning

**Goal**: Evaluate a personalized federated learning algorithm using Flower, showing its possible advantages over the FedAvg algorithm.

#### Choose one of the two proposed personalization algorithms:

1. **Federated Learning with Personalization Layers (FedPer)**

   - **Overview**: FedPer implements personalization by allowing some neural network layers to be client-specific, making the model closer to individual data distributions.
   - **Instructions**: Follow the tutorial on FedPer available at [Flower's documentation](https://flower.ai/docs/baselines/fedper.html).


2. **Model-Agnostic Meta-Learning for Personalized Federated Learning (FedMeta)**

   - **Overview**: FedMeta adapts the MAML algorithm for federated settings.
   - **Implementation**: Explore the concept of MAML in federated learning by following the [FedMeta tutorial](https://flower.ai/docs/baselines/fedmeta.html).

### Evaluation

- Reproduce the tutorial and compare the results with FedAvg to highlight the benefits of personalization.

---

Good luck, and don't hesitate to ask questions and collaborate with your peers!

At the end of the lesson, you can send your document and code to: [francesco.diana@inria.fr](mailto:francesco.diana@inria.fr)